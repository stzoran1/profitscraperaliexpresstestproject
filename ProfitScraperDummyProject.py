#Version 1.0
#Date: 07.11.2018 19:30
#Created by: Zoran Stankovic

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
#time is used just for testing and debugging
import time

class AliExpress:
    
    #On init start browser and go to AliExpress web page
    def __init__(self):
        self.driver = webdriver.Chrome()
        self.driver.get("https://www.aliexpress.com")

    def navigate_to_log_in(self):
        #Wait for page to load and open sign in dropdown in order to click on link
        divUserAccount = WebDriverWait(self.driver, 30).until(EC.presence_of_element_located((By.ID, "nav-user-account")))
        divUserAccount.click()

        lnkSignIn = divUserAccount.find_element(By.LINK_TEXT ,"Sign in")
        lnkSignIn.click()

    def close_promotion(self):
        #Sometimes promotion offer is showed and we need to close it in order to continue
        try:
            btnClosePromotion = WebDriverWait(self.driver, 3).until(EC.presence_of_element_located((By.CLASS_NAME, "close-layer")))
            btnClosePromotion.click()
        except:
            pass

    def log_in(self, username, password):
        self.username = username
        self.password = password

        self.navigate_to_log_in()

        #Log in form is inside iframe so we need first to switch to this 
        frmLogIn = WebDriverWait(self.driver, 30).until(EC.presence_of_element_located((By.ID, "alibaba-login-box")))

        self.driver.switch_to.frame(frmLogIn)
        txtUserName = WebDriverWait(self.driver, 30).until(EC.presence_of_element_located((By.ID, "fm-login-id")))
        txtPassword = self.driver.find_element_by_id("fm-login-password")

        txtUserName.send_keys(username)

        txtPassword.send_keys(password)

        btnLogIn = self.driver.find_element_by_id("fm-login-submit")
        btnLogIn.click()

        #Switch to main page frame
        self.driver.switch_to_default_content()
    
    #Country is in lowercase (us, uk, ca, rus)
    #Currency is uppercase (USD, GBP, RUB)
    def set_region(self, country, currency):
        #Open region panel
        divRegionPanel = WebDriverWait(self.driver, 30).until(EC.presence_of_element_located((By.CSS_SELECTOR, '[data-role="region-pannel"]')))
        lnkRegionPanel = divRegionPanel.find_element_by_id("switcher-info")
        lnkRegionPanel.click()

        #Click on country dropdown to generate links
        selCountry = WebDriverWait(self.driver, 30).until(EC.presence_of_element_located((By.CLASS_NAME, "link-fake-selector")))
        selCountry.click()
        
        listCountry = WebDriverWait(self.driver, 30).until(EC.presence_of_element_located((By.CLASS_NAME, "list-container")))
        #Find desired link and click on it
        spanCountry = listCountry.find_element(By.CSS_SELECTOR, ".css_flag.css_" + country)
        spanCountry.click()

        #Click on currency dropdown to generate links
        divCurrency = WebDriverWait(self.driver, 30).until(EC.presence_of_element_located((By.CSS_SELECTOR, '[data-role="switch-currency"]')))
        divCurrency.click()

         #Find desired link and click on it
        selCurrency = WebDriverWait(self.driver, 30).until(EC.presence_of_element_located((By.CSS_SELECTOR, '[data-currency="' + currency + '"]')))
        selCurrency.click()

        #Save changes
        btnSave = divRegionPanel.find_element(By.CSS_SELECTOR, '[data-role="save"]')
        btnSave.click()

    def search(self, criteria):
        #Self descriptive code
        txtSearch = WebDriverWait(self.driver, 30).until(EC.presence_of_element_located((By.ID, "search-key")))
        txtSearch.send_keys(criteria)

        btnSearch = self.driver.find_element_by_class_name("search-button")
        btnSearch.click()

    def get_search_results(self):
        try:
            #Try to find search results container if there is search results
            divSearchResults = WebDriverWait(self.driver, 30).until(EC.presence_of_element_located((By.ID, "hs-below-list-items")))

            searchResults = divSearchResults.find_elements(By.TAG_NAME, "li")

            return searchResults

        except:
            print("No search results")
            return -1

    def click_on_first(self):
        try:
            searchResults = self.get_search_results()

            searchResults[0].click()

        except:
            print("No search results")
        

    def __del__(self):
        self.driver.quit()

print("Starting...")

print("Navigating...")

aliExpressObj = AliExpress()

print("Login in...")

aliExpressObj.log_in("stasicevadokumentacija@gmail.com", "test1234")

#If promotion modal is showed close it
aliExpressObj.close_promotion()

print("Setting region")
aliExpressObj.set_region("uk","GBP")

print("Searching")
aliExpressObj.search("Boys Bicycle")

#If promotion modal is showed close it
aliExpressObj.close_promotion()

#Click on first
aliExpressObj.click_on_first()

print("Done")
#Just for debugging
time.sleep(500)